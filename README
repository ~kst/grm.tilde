git repo manager~
=================

+------v
| NOTE
+-------
| This is the tilde edition of git repo manager. If you want to host git
| repositories on your own server, see the master branch [1].
+------^

grm is a minimal, POSIX-compliant shell script for managing git repositories on
self-hosted git servers. The *tilde edition* of grm is a fork specially
designed for the users of pubnix/tildes servers [2]. It is intended to be used
with stagit [3] to generate a static web interface for the repos, but you don't
necessarily need it for this script to work.

Installation
------------

Either clone this repository, or use curl/wget to download the latest version
from the url below to your ~/bin.

  $ curl -o ~/bin/grm https://tilde.team/~kst/bin/grm
  $ chmod +x ~/bin/grm

If you want to have a web interface for your repositories, you should also have
stagit [3] compiled and installed to your ~/bin.

Then, edit the script to fill in some configurations,

  # root directory of git repositories
  GRM_REPOS_ROOT="$HOME/public_html"

  # default url prefix (without ending slash)
  GRM_URL_PREFIX="https://tilde.team/~$USER"

  # path of the post-receive hooks for stagit
  GRM_POSTRECV_HOOK="$HOME/bin/stagit-post-receive"

  # root directory of stagit web pages
  STAGIT_WEB_ROOT="$HOME/public_html/git"

An example of the post-receive hook for stagit can be found here [4]. Note that
the hook itself needs some further configuration.

For the default settings, your home directory will have the following structure

~
├── bin
│   ├── grm
│   ├── stagit
│   ├── stagit-index
│   ├── stagit-post-receive
│   └── ...
├── public_html
│   ├── git                  <-- your stagit root directory
│   │   ├── index.html
│   │   └── ...
│   ├── public-repo.git
│   │   ├── branches
│   │   ├── head
│   │   └── ...
│   └── unlisted-repo.git    <-- won't appear on the index page of stagit
│       ├── branches
│       ├── HEAD
│       ├── ...
│       └── stagit-no-index
└── ...

Usage
-----

$ grm help
usage: grm <command> [<args>]

git repo manager, tilde edition

commands:
    new                  create a new repo
    info repo_name       display metadata of the repo
    ls                   list all repos
    rm repo1 [repo2..]   remove repos
    rc                   recompile stagit index
    rc repo1 [repo2..]   recompile stagit pages for repos,
                         and recompile index
    rca                  recompile all repos
    help                 show help

Examples
--------

$ grm new
repo name
> grmr
no index? [y/N]
> n
description [a work in progress]
> grm redux
owner [username]
>
clone url [https://tilde.team/~username/grmr.git]
>
Initialized empty Git repository in /home/username/public_html/grmr.git/
writing stagit metadata...
installing stagit post-receive hook...
installing post-update hook for updating server info...
updating server info for the first time...
done!

$ grm ls
grm
grmr
$ grm info grmr
name: grmr
indexed: yes
description: grm redux
owner: username
url: https://tilde.team/~username/grmr.git

$ grm rc grmr
[grmr] recompiling stagit pages...
[index] rebuilding index...
[grmr] done!
[index] done!
recompilation done!

$ grm rm grmr
remove grmr? [y/N] y
[index] rebuilding index...
[index] done!

$ grm ls
grm

License
-------

grm is licensed under the MIT license.

[1]: https://sink.krj.st/grm/
[2]: https://tilde.wiki/other-tildes
[3]: https://codemadness.org/git/stagit/
[4]: https://tilde.team/~kst/git/stagit-postrecv/file/stagit-post-receive.html
